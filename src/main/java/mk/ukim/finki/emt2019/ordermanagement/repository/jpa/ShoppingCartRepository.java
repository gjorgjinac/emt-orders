package mk.ukim.finki.emt2019.ordermanagement.repository.jpa;

import mk.ukim.finki.emt2019.ordermanagement.model.Quantity;
import mk.ukim.finki.emt2019.ordermanagement.model.ShoppingCart;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author Riste Stojanov
 */
public interface ShoppingCartRepository extends JpaRepository<ShoppingCart, Long> {


    @Query("SELECT oi.quantity " +
            "FROM ShoppingCart sc INNER JOIN sc.items oi " +
            "WHERE oi.product.productId=:productId")
    Stream<Quantity> getReservedQuantityForProduct(@Param("productId") Long productId);


    ShoppingCart save(ShoppingCart shoppingCart);

    ShoppingCart findByAccount_AccountNumber(String accountNumber);

    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH,
            attributePaths = {"account", "items"})
    @Query("select c from ShoppingCart c where c.cartId=:cartId")
    Optional<ShoppingCart> fetchById(@Param("cartId") Long cartId);

    @EntityGraph(attributePaths = {"account", "items"})
    Optional<ShoppingCart> findWithAccountAndItemsByCartId(Long cardId);
}
