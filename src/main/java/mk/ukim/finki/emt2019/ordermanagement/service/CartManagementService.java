package mk.ukim.finki.emt2019.ordermanagement.service;

import mk.ukim.finki.emt2019.ordermanagement.model.Quantity;
import mk.ukim.finki.emt2019.ordermanagement.model.ShoppingCart;
import mk.ukim.finki.emt2019.ordermanagement.model.exceptions.*;

/**
 * @author Riste Stojanov
 */
public interface CartManagementService {


    ShoppingCart addItemToNewCart(Quantity quantity, Long productId)
            throws InvalidProductException, InvalidQuantityException, NotEnoughProductQuantityException;

    ShoppingCart addItemToNewLinkedCart(Long accountId, Quantity quantity, Long productId)
            throws InvalidAccountException, InvalidProductException, InvalidQuantityException, NotEnoughProductQuantityException;

    ShoppingCart addItemToCart(Long cartId, Quantity quantity, Long productId)
            throws InvalidCartException, InvalidProductException, InvalidQuantityException, NotEnoughProductQuantityException;

    ShoppingCart linkCartToAccount(Long cartId, Long accountId)
            throws InvalidCartException, InvalidAccountException,
            AccountLinkedToAnotherCartException;

    ShoppingCart mergeCartForAccount(Long cartId, Long accountId)
            throws InvalidCartException, InvalidAccountException;

    ShoppingCart markCartAsOverfilled(Long cartId)
            throws InvalidCartException;

    ShoppingCart changeItemQuantity(Long cartId, Quantity newQuantity, Long productId)
            throws InvalidCartException, InvalidProductException,
            InvalidQuantityException, NoProductInCartException, NotEnoughProductQuantityException;

    ShoppingCart removeItemFromCart(Long cartId, Long productId)
            throws InvalidCartException, InvalidProductException, NoProductInCartException;

    ShoppingCart expireCart(Long cartId) throws InvalidCartException, InvalidExpiryTimeException;

    //TODO: think again about their definition later
    ShoppingCart clearCart(Long cartId) throws InvalidCartException;

    ShoppingCart checkoutCart(Long orderId, Long accountId) throws InvalidCartException, InvalidAccountException;


}
